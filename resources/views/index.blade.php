@extends('layouts.app')

@section('content')

<div class="ld">
    <div class="ld-content">
        <div class="container ld-container">
            <div class="row">
                <div class="col-md-6 col-12 justify-content-center my-auto">
                    <div class="ld-content-left">
                        <h1>Earn Cash Paid Daily Selling Products!</h1>
                        <aside class="ld-video">
                            <button class="ld-video-cta sb-cta">Play</button>
                                See how in 15 seconds
                        </aside>
                    </div>
                </div>
                <div class="col-md-6 col-12 justify-content-center my-auto">
                    <div class="ld-content-right">
                        <div class="ld-form-wrapper has-signup-bonus">
                            <div class="ld-signup-bonus">
                                <div class="ld-signup-bonus-inner">
                                    <span class="ld-bonus-amount"><sup>&#36;</sup>1000</span>
                                    <span class="ld-bonus-word">Bonus</span>
                                    <sup class="ld-bonus-asterisk">*</sup>
                                </div>
                            </div>
    
                            <form class="ld-form">
                                <h2 class="ld-form-title">
                                    Join for Free
                                </h2>
                                <button class="btn mt-16">Sign Up Now</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="ld-content">
        <div class="container ld-content-reason">
            <div class="row m0 bg-white border-b">
                <div class="col-md-12 pt-16">
                    <h3>Reasons To Join Take Charge NXT</h3>
                </div> 
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="ld-reason-item">
                        <div class="ld-reason-item-icon">
                            <i class="fa fa-money-bill-alt"></i>
                        </div>
                        <p>
                            Daily Payouts
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="ld-reason-item">
                        <div class="ld-reason-item-icon">
                            <i class="fa fa-dollar-sign"></i>
                        </div>
                        <p>
                            Uncapped Referral Bonuses
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="ld-reason-item">
                        <div class="ld-reason-item-icon">
                            <i class="fa fa-gift"></i>
                        </div>
                        <p>
                            Monthly Giveaways
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-12">
                    <div class="ld-reason-item">
                        <div class="ld-reason-item-icon">
                            <i class="fa fa-trophy"></i>
                        </div>
                        <p>
                            Monthly Tournament Bonuses
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-12">
                    <div class="ld-reason-item">
                        <div class="ld-reason-item-icon">
                            <i class="fa fa-sort-amount-up"></i>
                        </div>
                        <p>
                            Sales Growth Opportunities
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="ld-content">
        <div class="container ld-counter">
            <div class="row m0 bg-white border-b">
                <div class="col-md-12 pt-16">
                    <h3>Take Charge NXT Pays out over</h3>
                </div>
                <div class="col-md-12 pb-16">
                    <div class="flip-counter-wrapper">
                        $<div class="flip-counter">
                            <span class="flip-counter-frame">
                                <span class="flip-counter-digit">1</span>
                            </span>
                            <span class="flip-counter-frame">
                                <span class="flip-counter-digit">0</span>
                            </span>
                            <span class="flip-counter-frame">
                                <span class="flip-counter-digit">0</span>
                            </span>,
                            <span class="flip-counter-frame">
                                <span class="flip-counter-digit">0</span>
                            </span>
                            <span class="flip-counter-frame">
                                <span class="flip-counter-digit">0</span>
                            </span>
                            <span class="flip-counter-frame">
                                <span class="flip-counter-digit">0</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="ld-content">
        <div class="container ld-testimonial">
            <div class="row m0 bg-grey pt-16 border-b">
                <div class="col-md-4 ld-testimonial-left">
                    <img src="{{asset('images/users/300_20.jpg')}}" class="rounded-circle" alt="Photo">
                </div>
                <div class="col-md-8 ld-testimonial-right">
                    <p class="testimonial">
                        “My most rewarding moment with Charge NXT is using the gift cards I earn to buy gifts for my child's Christmas and February birthday.”<br>
                        <span class="testimonial-meta">
                            <span class="testimonial-meta-name">Kim,</span>
                            Member since 2019
                        </span>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="ld-content">
        <div class="container ld-testimonial">
            <div class="row m0 bg-white border-b">
                <div class="col-md-12">
                    <div class="ld-content-gc">
                        <p>
                            Commissions Along with Free Gifts <em>Every Month</em>
                        </p>
                    </div>  
                </div>
            </div>
        </div>
    </div>

    <div class="ld-content">
        <div class="container ld-testimonial">
            <div class="row m0 bg-grey">
                <div class="col-md-12">
                    <div class="ld-content-signup">
                        <h2>Earn Cash Paid Daily Selling Products!</h2>
                        <button class="btn btn-brand btn-behance ld-content-signup-cta">Sign Up Now</button>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
